﻿using System;
namespace ModuloBaseSM22.Interfaces
{
    public interface IRepositorioPersonas
    {
        void ObtenerPersonas();
    }

    public class RepositorioPersonaDB : IRepositorioPersonas
    {
        public void ObtenerPersonas()
        {
            Console.WriteLine("Obteniendo personas de la base de datos...");
        }
    }

    public class RepositorioPersonaMemoria : IRepositorioPersonas
    {
        public void ObtenerPersonas()
        {
            Console.WriteLine("Obteniendo personas de la memoria...");
        }
    }
}
